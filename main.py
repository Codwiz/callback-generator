"""
Developers © 2022 by CodWiz is licensed under Attribution-NonCommercial-NoDerivatives 4.0 International. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
"""

import hashlib
import random
from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from typing import List

bot = Bot(token='token')
dp = Dispatcher(bot)

messages = {}

def generate_random_callback(user_id: int, button_num: int) -> str:
    random_str = ''.join(random.choices('abcdefghijklmnopqrstuvwxyz', k=8))
    md5 = hashlib.md5(str(user_id).encode('utf-8'))
    md5.update(random_str.encode('utf-8'))
    return f'fosya_{md5.hexdigest()}_{button_num}_{user_id}'

def create_keyboard(button_names: List[str], button_messages: List[str], num_buttons: int = 4) -> types.InlineKeyboardMarkup:
    buttons = []
    for i in range(num_buttons):
        try:
            cb = generate_random_callback(button_names[i], i + 1)
            buttons.append(cb)
        except IndexError:
            buttons.append(None)
    keyboard = types.InlineKeyboardMarkup()
    for i, cb in enumerate(buttons):
        if cb is not None and i < len(button_names):
            keyboard.add(types.InlineKeyboardButton(text=button_names[i],
                                                    callback_data=cb))
    return keyboard

@dp.message_handler(commands=['help'])
async def help_handler(message: types.Message):
    keyboard = create_keyboard(button_names=['Помощь', 'Команды', 'Профиль', 'О боте'],
   button_messages=['Тут будет информация о помощи', 
                     'Тут будут перечислены доступные команды', 
                     'Тут будет информация о пользователе', 
                     'Тут будет информация о боте'])
    await message.answer('Нажми на нужную кнопку для получения информации:', reply_markup=keyboard)

@dp.callback_query_handler(lambda c: c.data and c.data.startswith('fosya_'))
async def process_callback_random(callback_query: types.CallbackQuery):
    user_id = callback_query.from_user.id
    button_names = ['Помощь', 'Команды', 'Профиль', 'О боте']
    button_messages = ['Тут будет информация о помощи',
                       'Тут будут перечислены доступные команды',
                       'Тут будет информация о пользователе',
                       'Тут будет информация о боте']
    num_buttons = 4
    buttons = []
    for i in range(num_buttons):
        cb = generate_random_callback(button_names[i], i+1)
        buttons.append(cb)
    keyboard = create_keyboard(button_names, button_messages, num_buttons)
    button_num = int(callback_query.data.split('_')[-2])
    text = button_messages[button_num-1]
    await callback_query.message.edit_text(text=text, reply_markup=keyboard)

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
